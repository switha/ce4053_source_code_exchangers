#include <os.h>
#include <stdio.h>
#include "os_cfg_app.h"

#define MAX_NODES 10
#define STACKSIZE 1024u

static OS_TCB     ReccuringTaskTCB;
static  CPU_STK      RecurringTaskStk[STACKSIZE];

//#define LOGTIME

/******************************************************************************
Structs
*******************************************************************************/
typedef struct recurring_task {
  OS_TCB      * p_tcb;
  CPU_INT32U    period;//ms
  OS_DEADLINE   rel_deadline;
  CPU_INT32U    release;
} recurring_task_t;

extern char buf[2000];
typedef struct treeNode
{
    void * data;
    CPU_INT32U release; //to speed up search
    struct treeNode * child[2];
    struct treeNode * next;
    CPU_INT08U red;
} treeNode_t;

typedef struct rbTree
{
    treeNode_t * root;
} rbTree_t;
 

/******************************************************************************
Function prototypes
*******************************************************************************/

treeNode_t * getMinValue(rbTree_t * root);
int removeNode(rbTree_t *tree, CPU_INT32U release);
int insertRB(rbTree_t * tree, void * data, CPU_INT32U release, treeNode_t * node);
treeNode_t * insertRBRoot(treeNode_t *root, void * data,CPU_INT32U release, treeNode_t * node);





/******************************************************************************
Local variables
*******************************************************************************/
recurring_task_t recurring_tasks[MAX_NODES];   //structs holding recurring task info
treeNode_t      treeNodes[MAX_NODES];           //structs holding tree node info
rbTree_t        tree;                           //root of tree
int             freeNodes[MAX_NODES];           //map of free nodes in treeNode array
int             recTaskInitialized = 0;         //boolean to make sure freeNodes was initialized
static int      tasks;                      //number of tasks in tree

/******************************************************************************
Helper functions
*******************************************************************************/
//Returns the release time of the task in recurring_task_t struct

int getReleaseTime(void * task)
{
  if (task == NULL) return 65535; 
  return ((recurring_task_t *) task)->release;
}

CPU_INT32U getPeriod(void * task)
{
  if (task == NULL) return ((CPU_INT32U) 0)-1; //INTEGER.MAX
  return (((recurring_task_t *) task)->period);
}

void setReleaseTime(void * task, CPU_INT32U nextReleaseTime)
{
  if (task== NULL) return;
  ((recurring_task_t *) task)->release = nextReleaseTime;
}

OS_DEADLINE getDeadline(void * task)
{
  if (task == NULL) return 0;
  return ((recurring_task_t *) task)->rel_deadline;
}

OS_TCB * getTCB(void * task)
{
  if (task == NULL) return NULL;
  return ((recurring_task_t *) task)->p_tcb;
}

treeNode_t * getNextAndRemove(treeNode_t * node){
  treeNode_t * next = node->next;
  node->next = NULL;
  return next;
}


/******************************************************************************
*******************************************************************************
RECURRING TASK FUNCTIONS
*******************************************************************************
*******************************************************************************/

/********************************************************
Releases recurring tasks
********************************************************/
void OSRecTaskRelease()
{
  OS_ERR err;
  while(DEF_TRUE){
#ifdef LOGTIME
    CPU_TS StartTime;
    CPU_TS StartTime2;
    StartTime = OS_TS_GET();
    StartTime2 = OS_TS_GET();
#endif
    treeNode_t * nextReleaseNode = getMinValue(&tree);
    //no tasks scheduled, delay for an hour (arbitrary, just a long time, since nothing is needed)
    if (nextReleaseNode == NULL)
    {
        OSTimeDly(3600 * OS_CFG_TICK_RATE_HZ ,OS_OPT_TIME_DLY,&err);
        continue;
    } 
    
    //called before release time of next task, update timer to next shortest release
    CPU_INT32U nextReleaseTime = getReleaseTime(nextReleaseNode->data);
    /*the OS_TICK is only 32b, thus when overflowing, if the dispatcher doesn't 
    get its time slice, all tasks will be delayed until next overflow!
    TODO FIX THIS it'll happen in like 1200 hours though, not relevant atm.
    */
    OS_TICK currentTime = OSTimeGet(&err);
    
    if (nextReleaseTime > currentTime) {
      OSTimeDly(nextReleaseTime, OS_OPT_TIME_MATCH, &err);
      continue;
    }
    recurring_task_t * task;
    CPU_INT32U period;
    
   removeNode(&tree, nextReleaseTime);

    while (nextReleaseNode != NULL) 
    {
      
      //Update and release the node
      task = nextReleaseNode->data;
      //store the current time
      period = getPeriod(task);
      setReleaseTime(task, getReleaseTime(task) + period);
      nextReleaseNode->release = getReleaseTime(task);
      OSRecTaskDeadlineSet(getTCB(task), currentTime + getDeadline(task), &err);
      OSTaskResume(getTCB(task), &err);
#ifdef LOGTIME
      StartTime+=OS_TS_GET();
      sprintf(buf + strlen(buf), "(RCT)Inserting %s into RdyHeap: %u\tCurrent Tick: %u\tTime %u\n", getTCB(task)->NamePtr,StartTime - StartTime2 - StartTime2, OS_TS_GET(), OSTimeGet(&err));
      StartTime = OS_TS_GET();
      StartTime2 = OS_TS_GET();
#endif
        
      insertRB(&tree, task, getReleaseTime(task), nextReleaseNode);
      
      nextReleaseNode = getNextAndRemove(nextReleaseNode);                                   
    }
    nextReleaseNode = getMinValue(&tree);
#ifdef LOGTIME
    printf(buf);
    buf[0] = 0;
#endif
    OSTimeDly(getReleaseTime(nextReleaseNode->data), OS_OPT_TIME_MATCH, &err);
    /*
    do{
      //release the task (don't need to reassign, but just to be sure)
      removeNode(&tree, nextReleaseTime);
      task = nextReleaseNode->data;
      //store the current time
      period = getPeriod(task);
      setReleaseTime(task, getReleaseTime(task) + period);
      nextReleaseNode->release = getReleaseTime(task);
      OSRecTaskDeadlineSet(getTCB(task), currentTime + getDeadline(task), &err);
      OSTaskResume(getTCB(task), &err);
      
      //set timer for next release
      
      insertRB(&tree, task, getReleaseTime(task), nextReleaseNode);
      
      nextReleaseNode = getMinValue(&tree);
    } while (getReleaseTime(nextReleaseNode->data) <= currentTime);
    
    //printf("Task Dispatch %u\tCurrent tick: %u\tTime %u\n", StartTime - StartTime2 - StartTime2 + OS_TS_GET(), OS_TS_GET(), OSTimeGet(&err));
    OSTimeDly(getReleaseTime(nextReleaseNode->data), OS_OPT_TIME_MATCH, &err);
    */
  }  
}

/*
Initializes the Recursive task dispatcher
You MUST call this before using RecTaskCreate!
*/
void OSRecTaskInitialize(){
 /*  
  CPU_TS StartTime;
    CPU_TS StartTime2;
    StartTime = OS_TS_GET();
    StartTime2 = OS_TS_GET();
    */
  //tree = NULL;
  tasks = 0;
  for(int i = 0; i < MAX_NODES; ++i)
  {
    treeNode_t * node = &treeNodes[i];
    node->data = NULL;
    node->child[0] = NULL;
    node->child[1] = NULL;
    node->next = NULL;
  }
  
  OS_ERR  err;
  OSTaskCreate((OS_TCB     *)&ReccuringTaskTCB, 
                  (CPU_CHAR   *)"RecurringTaskDispatcher", 
                  (OS_TASK_PTR ) OSRecTaskRelease, 
                  (void       *) 0, 
                  (OS_PRIO     ) 4, 
                  (CPU_STK    *)&RecurringTaskStk[0], 
                  (CPU_STK_SIZE) STACKSIZE / 10u, 
                  (CPU_STK_SIZE) STACKSIZE, 
                  (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, 
                  (void       *)(CPU_INT32U) 1, 
                  (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                  (OS_ERR     *)&err,
                  0,
                  (CPU_BOOLEAN) 1);
  
  recTaskInitialized = 1;
//  StartTime+=OS_TS_GET();
  //printf("Recursive task initialization %u\tCurrent Tick: %u\tTime %u\n", StartTime - StartTime2 - StartTime2, OS_TS_GET(), OSTimeGet(&err));
   
}

void OSRecTaskSynchronousRelease(){
  OS_ERR err;
  OS_TICK delay = 1;
  OS_TICK release_time = OSTimeGet(&err) + delay;
  treeNode_t * p_node;
    p_node = tree.root;
    p_node->release = release_time;
    void * task = p_node->data;
    //OS_StartTask(p_tcb);
    setReleaseTime(task, release_time);

  
  for (int i = 0; i < MAX_NODES; i++){
    treeNode_t node = treeNodes[i];
    if (node.data == NULL) {continue;}

    node.release = release_time;
    task = node.data;
    OS_TCB * p_tcb = getTCB(task);
    OS_StartTask(p_tcb);
    setReleaseTime(task, release_time);
  }
  //resume dispatcher
    OSTimeDlyResume(&ReccuringTaskTCB, &err);

}


/********************************************************
Creates a recurring task 
********************************************************/
void OSRecTaskCreate(OS_TCB        *p_tcb,
                    CPU_CHAR      *p_name,
                    OS_TASK_PTR    p_task,
                    void          *p_arg,
                    OS_PRIO        prio,
                    CPU_STK       *p_stk_base,
                    CPU_STK_SIZE   stk_limit,
                    CPU_STK_SIZE   stk_size,
                    OS_MSG_QTY     q_size,
                    OS_TICK        time_quanta,
                    void          *p_ext,
                    OS_OPT         opt,
                    OS_ERR        *p_err,
                    OS_DEADLINE deadline,
                    CPU_INT32U      period)
{
  //create the task
   /* CPU_TS StartTime;
    CPU_TS StartTime2;
    StartTime = OS_TS_GET();
    StartTime2 = OS_TS_GET();
  */
  OS_ERR err;
    if (tasks >= MAX_NODES) return; //http://www.nooooooooooooooo.com/

  OSTaskCreate(p_tcb, p_name, p_task, p_arg, prio, p_stk_base, stk_limit, stk_size, q_size, time_quanta, p_ext, opt, p_err, deadline,0);
  //add the tcb to our datastructure
  recurring_task_t * task = &recurring_tasks[tasks];
  task->p_tcb = p_tcb;
  task->period = period;
 // task->release = OSTimeGet(&err) + period ;
  task->release = ((CPU_INT32U)0)-1; //task is not to be executed until synchronous release;
  
  //todo replace rel_deadline in task with p_tcb preemptlevel
  task->rel_deadline = deadline;
  p_tcb->PreemptLevel = deadline/1000;
  if (tasks == 0) {
    treeNode_t * root = insertRBRoot(NULL, task, getReleaseTime(task), NULL);
    tree.root = root;
  } else {
    insertRB(&tree, task, getReleaseTime(task), NULL);
  }
  //StartTime+=OS_TS_GET();
  //printf("Recursive task Creation %u\tCurrent Time: %u\n", StartTime - StartTime2 - StartTime2, OS_TS_GET());
  //delay until it is reached
   // OSTimeDlyResume(&ReccuringTaskTCB, p_err);
  
}

/********************************************************
Deletes a recurring task
********************************************************/
void OSRectTaskDel(OS_TCB *p_tcb)
{
#warning need to update removeNode to remove by p_tcb and not release time
 // removeNode(&tree, p_tcb);
}

/********************************************************
********************************************************
Tree Structure
********************************************************
********************************************************/
treeNode_t * getFreeNode()
{
  if (tasks >= MAX_NODES - 1) {
    return NULL;
  }
  return &treeNodes[tasks++];

}

treeNode_t * createNode(void * data, int release)
{
    treeNode_t * newNode = getFreeNode();
   
    if(newNode!=NULL){
    newNode->data = data;
    newNode->release = release;
    newNode->red = 1;
    newNode->child[0] = NULL;
    newNode->child[1] = NULL;
    }
    return newNode;
}
/*Checks whether the node is red*/
int isRed(treeNode_t * root)
{
    return root != NULL && root->red == 1;
}
/*Rotations*/
treeNode_t * firstRot(treeNode_t * root, int dir)       //rotates around the root
{
    treeNode_t * temp = root->child[!dir];        //dir=0 ->h�ger rot
    root->child[!dir] = temp->child[dir];
    temp->child[dir] = root;
    root->red = 1;
    temp->red = 0;
    return temp;
}
 
treeNode_t * secondRot(treeNode_t * root, int dir)
{
    root->child[!dir] = firstRot(root->child[!dir], !dir);
    return firstRot(root, dir);
}
 
/*Violations*/
int evaluate(treeNode_t *root)
{
    int lh, rh;
 
    if (root == NULL)
    {
        return 1;
    }
    else
    {
        treeNode_t *ln = root->child[0];
        treeNode_t *rn = root->child[1];
 
        /* Consecutive red links */
        if (isRed(root))
        {
            if (isRed(ln) || isRed(rn))
            {
                puts("Red violation");
                return 0;
            }
        }
 
        lh = evaluate(ln);
        rh = evaluate(rn);
 
        /* Invalid binary search tree */
        if ((ln != NULL && ln->release >= root->release) || (rn != NULL && rn->release <= root->release))
        {
            puts("Binary tree violation");
            return 0;
        }
 
        /* Black height mismatch */
        if (lh != 0 && rh != 0 && lh != rh)
        {
            puts("Black violation");
            return 0;
        }
 
        /* Only count black links */
        if (lh != 0 && rh != 0)
        {
            return isRed(root) ? lh : lh + 1; //if root is red return lh else lh+1
        }
        else
        {
            return 0;
        }
    }
}
 /*
  node is optional, use it to not allocate more memory
*/
treeNode_t * insertRBRoot(treeNode_t *root, void * data, CPU_INT32U release, treeNode_t * node)
{
 
    if(root==NULL)
    {
      if (node == NULL){
        root=createNode(data,release);
      } else {
        root = node;
        node->red = 1;
      }
      
    
    }
    else //if (release!=root->release)
    {
        int dir = root->release <= release;
        root->child[dir]=insertRBRoot(root->child[dir],data,release, node);
        if(isRed(root->child[dir]))
        {
            if (isRed(root->child[!dir])){
                root->red=1;
                root->child[0]->red=0;
                root->child[1]->red=0;
            }
            else
            {
                if(isRed(root->child[dir]->child[dir]))
                {
                    root=firstRot(root,!dir);
                }
                else if (isRed(root->child[dir]->child[!dir]))
                {
                    root=secondRot(root,!dir);
                }
            }
        }
    }
 
    return root;
}
//Inserts a node into the next list of the node
int insertIntoNode(treeNode_t * node, void * data, int release, treeNode_t * newNode)
{
  if (node == NULL) { return 0;}
  while (node->next != NULL){
    node = node->next;
  }
  node->next = newNode == NULL ? createNode(data, release) : newNode;
  if (node->next == NULL) {
    return 0;
  }
  return 1;
}

//finds a node with the same release time in the tree, and inserts a node into it's next list
int findAndInsertIntoExistingNode(treeNode_t * root, CPU_INT32U release, void * data, treeNode_t * node) {
  
  while (1) {
    if (root == NULL) { return 0; }
    if (root->release == release){
      return insertIntoNode(root, data, release, node);
    }
    if (root->release > release) { // go left
        root = root->child[0];
    } else {
        root = root->child[1];
    }
  
  } 
  
  //return 0;
}
 
int insertRB(rbTree_t * tree, void * data, CPU_INT32U release, treeNode_t * node){
  if (findAndInsertIntoExistingNode(tree->root, release, data, node)) {
    return 1;
  }
    tree->root=insertRBRoot(tree->root, data, release, node);
    tree->root->red=0;
    return 1;
}

int removeNode(rbTree_t *tree, CPU_INT32U release)
{
    if (tree->root!=NULL)
    {
        treeNode_t head = {0}; /*False tree root*/
        treeNode_t *q, *p, *g; /*Helpers*/
        treeNode_t *f = NULL; /*found item*/
        int dir = 1;
 
        /*set up helpers*/
        q=&head;
        g=p = NULL;
        q->child[1] = tree->root;
 
        /*search & let a red go down to the tree*/
        while (q->child[dir] != NULL)
        {
            int last = dir;
 
            /*update le helpers*/
            g=p,p=q;
            q=q->child[dir];
            dir = q->release < release;
            /*save found note*/
            if (q->release == release)
            {
                f=q;
            }
 
            /*push the red node down*/
            if (!isRed(q) && !isRed(q->child[dir]))
            {
                if (isRed(q->child[!dir]))
                {
                    p=p->child[last] = firstRot(q, dir);
                }
                else if (!isRed(q->child[!dir]))
                {
                    treeNode_t *s = p->child[!last];
                    if( s != NULL)
                    {
                        if (!isRed(s->child[!last]) && !isRed(s->child[last]))
                        {
                            /*switch the colour*/
                            p->red = 0;
                            s->red = 1;
                            q->red = 1;
                        }
                        else
                        {
                            int dir2 = g->child[1] == p;
                            if(isRed(s->child[last]))
                            {
                                g->child[dir2] = secondRot(p, last);
                            }
                            else if (isRed(s->child[!last]))
                            {
                                g->child[dir2] = firstRot(p, last);
                            }
                            //makin sure its right colouring
                            q->red = g->child[dir2]->red = 1;
                            g->child[dir2]->child[0]->red = 0;
                            g->child[dir2]->child[1]->red = 0;
                        }
                    }
                }
            }
        }
        if (f != NULL)
        {
            f->release = q->release;
            p->child[p->child[1]==q] = q->child[q->child[0] == NULL];
            //free(q);
        }
        tree->root = head.child[1];
        if (tree->root != NULL)
        {
            tree->root->red = 0;
        }
    }
    return 1;
}

/*
 Returns the node with smallest value
 */
treeNode_t * getMinValue(rbTree_t * tree)
{
   
    treeNode_t * root = tree->root;
     if (root == NULL) 
    {
        return NULL;
    }
    while (root->child[0] != NULL) {
        root = root->child[0];
    }
    return root;
}