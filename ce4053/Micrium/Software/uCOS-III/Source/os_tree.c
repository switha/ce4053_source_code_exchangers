#include <os.h>

#define NULL ((void *) 0)
#define MAX_NODES 10

typedef struct treeNode
{
    void * data;
    struct treeNode * left;
    struct treeNode * right;
} treeNode_t;

///TREE NODES

treeNode_t treeNodes[MAX_NODES];


int freeNodeNum = 0;
/*
    @param data: pointer to the data structure to be added
    @param (* valueOf): function that returns the value with which tree structs can be compared
 */
//TODO convert from returning treeNode_t * to passing treeNode_t ** root and change the root itslef ...
treeNode_t * addNode(void * data, int (* valueOf)(void * obj), treeNode_t * root)
{
    if (root == NULL)
    {
        root = &treeNodes[freeNodeNum];
        ++freeNodeNum;
        root->data = data;
        return root;
    }
    
    //duplicate values are stored as 'larger' to keep order of adding
    //currently all nodes  in the path to the new node are readded every time
    if (valueOf(data) < valueOf(root->data))
    {
        root->left = addNode(data, valueOf, root->left);
    } else 
    {
        root->right = addNode(data, valueOf, root->right);
    }
}

//TODO REMOVE CODE DUPLICATION
/*
 * Since we're looking for the min val, i.e. leftmost child, only 4 cases can occur:
 * 1) the root is the leftmost node and has no children, it is removed completely
 * 2) the root is the leftmost node and has one child, thus its right child will become root
 * 2) the leftmost child has no children, can be removed
 * 3) the leftmost child has one right child, thus the right child is reassigned as the left child
 */
treeNode_t * removeMinNode(int (* valueOf)(void * data), treeNode_t ** root)
{
    if (root == NULL || (*root) == NULL)
    { return (* root); }
    
    if ((*root)->left == NULL)
    {   //remove root
        void * node = (*root);
        if ((*root)->right != NULL) 
             { *root = (*root)->right; } 
        else { *root = NULL;  }
        //free(*root);
        return node;
    }
    treeNode_t * node = (*root);
    treeNode_t * prev;
    while (node->left != NULL) 
    {
        prev = node;
        node = node->left;
    }
    
    if (node->right != NULL)
    {
        prev->left = node->right;
    } else
    {
        prev->left = NULL;
    }
    //free(node);
    return node;
    
}

/*
 Returns the node with smallest value
 */
treeNode_t * getMinValue(treeNode_t * root)
{
    if (root == NULL) 
    {
        return root;
    }
    while (root->left != NULL) {
        root = root->left;
    }
    return root;
}

void printTreeInOrder(int (*valueOf)(void * obj), treeNode_t * root)
{
    if (root == NULL)
    { return; }
    printTreeInOrder(valueOf, root->left);
    printf("Value: %d\n", valueOf(root->data));
    printTreeInOrder(valueOf, root->right);
}
