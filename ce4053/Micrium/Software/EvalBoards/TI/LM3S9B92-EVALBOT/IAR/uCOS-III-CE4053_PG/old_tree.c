/*
///TREE NODES
//moves a removed node to a new position based on valueOf the data
treeNode_t * changeNode(void * data, int (* valueOf)(void * obj), treeNode_t * root, treeNode_t * node)
{
    if (root == NULL)
    {
        root = node;
        root->data = data;
        return root;
    }
    treeNode_t * current = root;
    //duplicate values are stored as 'larger' to keep order of adding
    //currently all nodes  in the path to the new node are readded every time
  
    
    while(DEF_TRUE){
      if (valueOf(data) < valueOf(current->data))
      {
        if(current->left !=NULL)
        {
          current = current->left;
        } else {
          current->left = node;
          break;
        }
      } else {
        if (current->right != NULL) 
        {
          current = current->right;
        } else {
          current->right = node;
          break;
        }
      }
    }
    return root;

    
}

//"allocates" memory for a completely new node
treeNode_t * addNode(void * data, int (* valueOf)(void * obj), treeNode_t * root)
{
    treeNode_t * node = &treeNodes[tasks];
    tasks++;
    node->data = data;
    if (root == NULL)
    {
        root = node;
        return root;
    }
    
    treeNode_t * current = root;
    while(DEF_TRUE){
      if (valueOf(data) < valueOf(current->data))
      {
        if(current->left !=NULL)
        {
          current = current->left;
        } else {
          current->left = node;
          break;
        }
      } else {
        if (current->right != NULL) 
        {
          current = current->right;
        } else {
          current->right = node;
          break;
        }
      }
    }
    return root;
}

//TODO REMOVE CODE DUPLICATION
/*
 * Since we're looking for the min val, i.e. leftmost child, only 4 cases can occur:
 * 1) the root is the leftmost node and has no children, it is removed completely
 * 2) the root is the leftmost node and has one child, thus its right child will become root
 * 2) the leftmost child has no children, can be removed
 * 3) the leftmost child has one right child, thus the right child is reassigned as the left child
 */
treeNode_t * removeMinNode(treeNode_t ** root)
{
    if (root == NULL || (*root) == NULL)
    { return (* root); }
    
    if ((*root)->left == NULL)
    {   //remove root
        treeNode_t * node = (*root);
        if ((*root)->right != NULL) 
             { 
               *root = (*root)->right; 
                node->right = NULL;
                node->left = NULL;
             } 
        else { *root = NULL;  }
        //free(*root);
        return node;
    }
    treeNode_t * node = (*root);
    treeNode_t * prev;
    while (node->left != NULL) 
    {
        prev = node;
        node = node->left;
    }
    
    if (node->right != NULL)
    {
        prev->left = node->right;
    } else
    {
        prev->left = NULL;
    }
    //free(node);
    node->left = NULL;
    node->right = NULL;
    return node;
    
}

/*
 Returns the node with smallest value
 */
treeNode_t * getMinValue(treeNode_t * root)
{
    if (root == NULL) 
    {
        return NULL;
    }
    while (root->left != NULL) {
        root = root->left;
    }
    return root;
}