/*
*********************************************************************************************************
*                                              EXAMPLE CODE
*
*                          (c) Copyright 2009-2010; Micrium, Inc.; Weston, FL
*
*               All rights reserved.  Protected by international copyright laws.
*               Knowledge of the source code may NOT be used to develop a similar product.
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                            EXAMPLE CODE
*
* Filename      : app.c
* Version       : V1.00
* Programmer(s) : FUZZI
*
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                             INCLUDE FILES
*********************************************************************************************************
*/

#include <includes.h>
#include <stdio.h>
#include "driverlib/timer.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include <os.h>
#include "bsp_io.h"

/*
*********************************************************************************************************
*                                             LOCAL DEFINES
*********************************************************************************************************
*/

#define ONESECONDTICK             7000000

#define TASK1PERIOD                   10
#define TASK2PERIOD                   20


#define WORKLOAD1                     3
#define WORKLOAD2                     2


#define TIMERDIV                      (BSP_CPUClkFreq() / (CPU_INT32U)OSCfg_TickRate_Hz)




/*
*********************************************************************************************************
*                                            LOCAL VARIABLES
*********************************************************************************************************
*/

static  OS_TCB       AppTaskStartTCB;
static  CPU_STK      AppTaskStartStk[APP_TASK_START_STK_SIZE];

static  OS_TCB       AppTaskLeftBlinkTCB;
static  CPU_STK      AppTask1Stk[APP_TASK_1_STK_SIZE];

static  OS_TCB       AppTaskRightBlinkTCB;
static  CPU_STK      AppTask2Stk[APP_TASK_2_STK_SIZE];

static  OS_TCB       AppTaskBlinkTCB;
static  CPU_STK      AppTask3Stk[APP_TASK_3_STK_SIZE];

static  OS_TCB       AppTaskMoveForwardTCB;
static  CPU_STK      AppTask4Stk[APP_TASK_4_STK_SIZE];

static  OS_TCB       AppTaskMoveBackwardTCB;
static  CPU_STK      AppTask5Stk[APP_TASK_5_STK_SIZE];

    

CPU_INT32U      iCnt = 0;
CPU_INT08U      Left_tgt;
CPU_INT08U      Right_tgt;
CPU_INT32U      iToken  = 0;
CPU_INT32U      iCounter= 1;
CPU_INT32U      iMove   = 10;
CPU_INT32U      measure=0;





/*
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*/

static  void        AppRobotMotorDriveSensorEnable    ();
        void        IntWheelSensor                    ();
        void        RoboTurn                          (tSide dir, CPU_INT16U seg, CPU_INT16U speed);

static  void        AppTaskStart                 (void  *p_arg);
static void         leftLEDBlink                   (void *p_arg);
static void         rightLEDBlink                  (void *p_arg);
static void         LEDBlink                   (void *p_arg);
static void         moveForward                   (void *p_arg);
static void         moveBackward                   (void *p_arg);



/*
*********************************************************************************************************
*                                                main()
*
* Description : This is the standard entry point for C code.  It is assumed that your code will call
*               main() once you have performed all necessary initialization.
*
* Arguments   : none
*
* Returns     : none
*********************************************************************************************************
*/

 int  main (void)
{
    OS_ERR  err;

    BSP_IntDisAll();                                            /* Disable all interrupts.                              */
    OSInit(&err);                                               /* Init uC/OS-III.                                      */

    OSTaskCreate((OS_TCB     *)&AppTaskStartTCB,           /* Create the start task                                */
                 (CPU_CHAR   *)"App Task Start",
                 (OS_TASK_PTR ) AppTaskStart,
                 (void       *) 0,
                 (OS_PRIO     ) APP_TASK_START_PRIO,
                 (CPU_STK    *)&AppTaskStartStk[0],
                 (CPU_STK_SIZE) APP_TASK_START_STK_SIZE / 10u,
                 (CPU_STK_SIZE) APP_TASK_START_STK_SIZE,
                 (OS_MSG_QTY  ) 0u,
                 (OS_TICK     ) 0u,
                 (void       *) (CPU_INT32U) 0, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR     *)&err,
                 (OS_DEADLINE) 0,
                 (CPU_BOOLEAN) 1);

    OSStart(&err);                                              /* Start multitasking (i.e. give control to uC/OS-III). */
}


/*
*********************************************************************************************************
*                                          STARTUP TASK
*
* Description : This is an example of a startup task.  As mentioned in the book's text, you MUST
*               initialize the ticker only once multitasking has started.
*
* Arguments   : p_arg   is the argument passed to 'AppTaskStart()' by 'OSTaskCreate()'.
*
* Returns     : none
*
* Notes       : 1) The first line of code is used to prevent a compiler warning because 'p_arg' is not
*                  used.  The compiler should not generate any code for this statement.
*********************************************************************************************************
*/

static  void  AppTaskStart (void  *p_arg)
{
    CPU_INT32U  clk_freq;
    CPU_INT32U  cnts;
    OS_ERR      err;
    (void)&p_arg;
    BSP_Init();                                                 /* Initialize BSP functions                             */
    CPU_Init();                                                 /* Initialize the uC/CPU services                       */
    clk_freq = BSP_CPUClkFreq();                                /* Determine SysTick reference freq.                    */
    cnts     = clk_freq / (CPU_INT32U)OSCfg_TickRate_Hz;        /* Determine nbr SysTick increments                     */
    OS_CPU_SysTickInit(cnts);                                   /* Init uC/OS periodic time src (SysTick).              */
    CPU_TS_TmrFreqSet(clk_freq);
    
    /* Enable Wheel ISR Interrupt */
    AppRobotMotorDriveSensorEnable();
    
    /* Initialise the 2 Main Tasks to  Deleted State */
    int fraction = 1;
    recTaskInitialize();
    OSRecTaskCreate((OS_TCB     *)&AppTaskLeftBlinkTCB,    (CPU_CHAR   *)"Left blink",  (OS_TASK_PTR ) leftLEDBlink, (void       *) 0, (OS_PRIO     ) APP_TASK_1_PRIO, (CPU_STK    *)&AppTask1Stk[0], (CPU_STK_SIZE) APP_TASK_1_STK_SIZE / 10u, (CPU_STK_SIZE) APP_TASK_1_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 1, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err,(OS_DEADLINE)  5000, (OS_PERIOD) 5000/fraction);                                              
    OSRecTaskCreate((OS_TCB     *)&AppTaskRightBlinkTCB,   (CPU_CHAR   *)"Right blink", (OS_TASK_PTR ) rightLEDBlink,  (void       *) 0, (OS_PRIO     ) APP_TASK_2_PRIO, (CPU_STK    *)&AppTask2Stk[0], (CPU_STK_SIZE) APP_TASK_2_STK_SIZE / 10u, (CPU_STK_SIZE) APP_TASK_2_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err,(OS_DEADLINE) 20000, (OS_PERIOD)20000/fraction);
    OSRecTaskCreate((OS_TCB     *)&AppTaskBlinkTCB,        (CPU_CHAR   *)"blink",       (OS_TASK_PTR ) LEDBlink, (void       *) 0, (OS_PRIO     ) APP_TASK_3_PRIO, (CPU_STK    *)&AppTask3Stk[0], (CPU_STK_SIZE) APP_TASK_3_STK_SIZE / 10u, (CPU_STK_SIZE) APP_TASK_3_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 1, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err,(OS_DEADLINE) 15000, (OS_PERIOD)15000/fraction);
    OSRecTaskCreate((OS_TCB     *)&AppTaskMoveForwardTCB,  (CPU_CHAR   *)"Fwd",         (OS_TASK_PTR ) moveForward,  (void       *) 0, (OS_PRIO     ) APP_TASK_4_PRIO, (CPU_STK    *)&AppTask4Stk[0], (CPU_STK_SIZE) APP_TASK_4_STK_SIZE / 10u, (CPU_STK_SIZE) APP_TASK_4_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err,(OS_DEADLINE) 35000, (OS_PERIOD)35000/fraction);
    OSRecTaskCreate((OS_TCB     *)&AppTaskMoveBackwardTCB, (CPU_CHAR   *)"bwd",         (OS_TASK_PTR ) moveBackward, (void       *) 0, (OS_PRIO     ) APP_TASK_5_PRIO, (CPU_STK    *)&AppTask5Stk[0], (CPU_STK_SIZE) APP_TASK_5_STK_SIZE / 10u, (CPU_STK_SIZE) APP_TASK_5_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 1, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err,(OS_DEADLINE) 60000, (OS_PERIOD)60000/fraction);
    OSRecTaskSynchronousRelease();

    
    /* Delete this task */
    OSTaskDel((OS_TCB *)0, &err);
    
}

static  void  AppTaskOne (void  *p_arg)
{ 
    OS_ERR      err;
    while(DEF_TRUE){
      if(iMove > 0)
      {
        if(iMove%2==0)
        {  
        RoboTurn(FRONT, 4, 50);
        iMove--;
        }
        else{
          RoboTurn(BACK, 4, 50);
          iMove++;
        }
      }
       OSTaskSuspend(NULL, &err);
    }

}



/*TODO refactor LEDId into p_arg*/
static void leftLEDBlink(void *p_arg)
{
  OS_ERR err;
  CPU_INT16U LEDId = 2u;
  while (DEF_TRUE) 
  {
    //printf(" LEFT ");
    BSP_LED_On(LEDId);
    OSTimeDlyHMSM(0,0,0,300, OS_OPT_TIME_DLY, &err);
    BSP_LED_Off(LEDId);
    OSTaskSuspend(NULL, &err);
  }
}
static void rightLEDBlink(void *p_arg)
{
  OS_ERR err;
  CPU_INT16U LEDId = 1u;
  while (DEF_TRUE) 
  {
    //printf(" RIGHT ");
    BSP_LED_On(LEDId);
    OSTimeDlyHMSM(0,0,0,300, OS_OPT_TIME_DLY, &err);
    BSP_LED_Off(LEDId);
    OSTaskSuspend(NULL, &err);
  }
}

static void LEDBlink(void *p_arg)
{
  OS_ERR err;
  CPU_INT16U LEDId = 0u;
  while (DEF_TRUE) 
  {
    //printf(" BLINK ");
    BSP_LED_On(LEDId);
    OSTimeDlyHMSM(0,0,0,300, OS_OPT_TIME_DLY, &err);
    BSP_LED_Off(LEDId);
    OSTaskSuspend(NULL, &err);
  }
}


static void moveForward(void *p_arg)
{
  OS_ERR err;
  CPU_INT32U i,j;
  while (DEF_TRUE) 
  {
    //printf(" FWD ");
    RoboTurn(FRONT, 4, 50);
      
      //It's a little cold here, so let's make some heat
     for(i=0; i <ONESECONDTICK; i++){
         j = ((i * 2)+j);
    }
     OSTaskSuspend(NULL, &err);
  }
}
static void moveBackward(void *p_arg)
{
  OS_ERR err;
  CPU_INT32U i,j;
  while (DEF_TRUE) 
  {
    //printf(" BWD ");
     RoboTurn(BACK, 4, 50);
     
     for(i=0; i <ONESECONDTICK; i++){
         j = ((i * 2)+j);
    }
     OSTaskSuspend(NULL, &err);
  }
}

static  void  AppRobotMotorDriveSensorEnable ()
{
    BSP_WheelSensorEnable();
    BSP_WheelSensorIntEnable(RIGHT_SIDE, SENSOR_A, (CPU_FNCT_VOID)IntWheelSensor);
    BSP_WheelSensorIntEnable(LEFT_SIDE, SENSOR_A, (CPU_FNCT_VOID)IntWheelSensor);
}


void IntWheelSensor()
{
	CPU_INT32U         ulStatusR_A;
	CPU_INT32U         ulStatusL_A;

	static CPU_INT08U CountL = 0;
	static CPU_INT08U CountR = 0;

	static CPU_INT08U data = 0;

	ulStatusR_A = GPIOPinIntStatus(RIGHT_IR_SENSOR_A_PORT, DEF_TRUE);
	ulStatusL_A = GPIOPinIntStatus(LEFT_IR_SENSOR_A_PORT, DEF_TRUE);

        if (ulStatusR_A & RIGHT_IR_SENSOR_A_PIN)
        {
          GPIOPinIntClear(RIGHT_IR_SENSOR_A_PORT, RIGHT_IR_SENSOR_A_PIN);           /* Clear interrupt.*/
          CountR = CountR + 1;
        }

        if (ulStatusL_A & LEFT_IR_SENSOR_A_PIN)
        {
          GPIOPinIntClear(LEFT_IR_SENSOR_A_PORT, LEFT_IR_SENSOR_A_PIN);
          CountL = CountL + 1;
        }

	if((CountL >= Left_tgt) && (CountR >= Right_tgt))
        {
          data = 0x11;
          Left_tgt = 0;
          Right_tgt = 0;
          CountL = 0;
          CountR = 0;
          BSP_MotorStop(LEFT_SIDE);
          BSP_MotorStop(RIGHT_SIDE);
        }
        else if(CountL >= Left_tgt)
        {
          data = 0x10;
          Left_tgt = 0;
          CountL = 0;
          BSP_MotorStop(LEFT_SIDE);
        }
        else if(CountR >= Right_tgt)
        {
          data = 0x01;
          Right_tgt = 0;
          CountR = 0;
          BSP_MotorStop(RIGHT_SIDE);
        }
        return;
}

void RoboTurn(tSide dir, CPU_INT16U seg, CPU_INT16U speed)
{
	Left_tgt = seg;
        Right_tgt = seg;

	BSP_MotorStop(LEFT_SIDE);
	BSP_MotorStop(RIGHT_SIDE);

        BSP_MotorSpeed(LEFT_SIDE, speed <<8u);
	BSP_MotorSpeed(RIGHT_SIDE,speed <<8u);

	switch(dir)
	{
            case FRONT :
                    BSP_MotorDir(RIGHT_SIDE,FORWARD);
                    BSP_MotorDir(LEFT_SIDE,FORWARD);
                    BSP_MotorRun(LEFT_SIDE);
                    BSP_MotorRun(RIGHT_SIDE);
                    break;
                    
            case BACK :
                    BSP_MotorDir(LEFT_SIDE,REVERSE);
                    BSP_MotorDir(RIGHT_SIDE,REVERSE);
                    BSP_MotorRun(RIGHT_SIDE);
                    BSP_MotorRun(LEFT_SIDE);
                    break;
                    
            case LEFT_SIDE :
                    BSP_MotorDir(RIGHT_SIDE,FORWARD);
                    BSP_MotorDir(LEFT_SIDE,REVERSE);
                    BSP_MotorRun(LEFT_SIDE);
                    BSP_MotorRun(RIGHT_SIDE);
                    break;
                    
            case RIGHT_SIDE:
                    BSP_MotorDir(LEFT_SIDE,FORWARD);
                    BSP_MotorDir(RIGHT_SIDE,REVERSE);
                    BSP_MotorRun(RIGHT_SIDE);
                    BSP_MotorRun(LEFT_SIDE);
                    break;
                    
            default:
                    BSP_MotorStop(LEFT_SIDE);
                    BSP_MotorStop(RIGHT_SIDE);
                    break;
	}

	return;
}
